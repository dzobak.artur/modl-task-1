﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsApp29
{
    public partial class Form1 : Form
    {
        private Operation operation;
        private DataTable dataTable1;
        public Form1()
        {
            InitializeComponent();
            operation = new Operation();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataTable1 = new DataTable();
            dataTable1.Columns.Add("Число", typeof(double));

            dataGridView1.DataSource = dataTable1;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            double[] sequence = GetSequenceFromDataGridView();
            int signChanges = operation.CountSignChanges(sequence);
            MessageBox.Show("Кількість змін знаку: " + signChanges);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox1.Text, out double number))
            {
                AddNumberToDataGridView(number);
                textBox1.Clear();
            }
            else
            {
                MessageBox.Show("Введіть коректне число.");
            }
        }
        private void AddNumberToDataGridView(double number)
        {
            dataTable1.Rows.Add(number);
        }
        private double[] GetSequenceFromDataGridView()
        {
            List<double> sequence = new List<double>();

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[0].Value != null && double.TryParse(row.Cells[0].Value.ToString(), out double number))
                {
                    sequence.Add(number);
                }
            }

            return sequence.ToArray();
        }

    }
}
