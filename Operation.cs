﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp29
{
    internal class Operation
    {
        public int CountSignChanges(double[] sequence)
        {
            int signChanges = 0;

            if (sequence.Length > 1)
            {
                double previousNumber = sequence[0];

                for (int i = 1; i < sequence.Length; i++)
                {
                    double currentNumber = sequence[i];

                    if (Math.Sign(previousNumber) != Math.Sign(currentNumber))
                    {
                        signChanges++;
                    }

                    previousNumber = currentNumber;
                }
            }

            return signChanges;
        }

    }
}
